from csv import reader
from models import Ctr, Dom, Var 


def clean_line(line):
    return [
        c for c in line if c != ''
    ]

def file_to_ctrs(filepath):
    with open(filepath) as file:
        ctr_reader = reader(file, delimiter=' ')
        filtered = [
            clean_line(line)
            for line in ctr_reader
        ]

    return [
        Ctr(first_var=line[0],
            second_var=line[1],
            constraint=line[2],
            operator=line[3],
            deviation=line[4],
            weight=(line[5] if len(line) > 5
                    else '0'))
        for line in filtered
    ]


def file_to_doms(filepath):
    with open(filepath) as file:
        dom_reader = reader(file, delimiter=' ')
        filtered = [ clean_line(line) for line in dom_reader ]

    return [
        Dom(index = index,
            cardinality = cardinality,
            frequencies = frequencies)
        for index, cardinality, *frequencies in filtered
    ]
    

def file_to_vars(filepath):
    with open(filepath) as file:
        var_reader = reader(file, delimiter=' ')
        filtered = [ clean_line(line) for line in var_reader ]

    return [
        Var(index=index,
            dom=dom) 
            if not optional else
        Var(index=index,
            dom=dom,
            initial=optional[0],
            mobility=optional[1])
        for index, dom, *optional in filtered
    ]