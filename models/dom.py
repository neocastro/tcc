import mongoengine as me 

class Dom(me.EmbeddedDocument):
    def __repr__(self):
        return str(self.to_mongo().to_dict())

    index = me.IntField()
    cardinality = me.IntField()
    frequencies = me.ListField(me.IntField())