import mongoengine as me
from . import Ctr, Dom, Var



class Graph(me.Document):
    def __repr__(self):
        return str(self.to_mongo().to_dict())
        
    name = me.StringField(primary_key=True)
    ctr = me.EmbeddedDocumentListField(Ctr)
    dom = me.EmbeddedDocumentListField(Dom)
    var = me.EmbeddedDocumentListField(Var)