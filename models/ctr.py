import mongoengine as me

constraints = {'D', 'C', 'F', 'P', 'L'}
operators = {'>', '='}


class Ctr(me.EmbeddedDocument):
    def __repr__(self):
        return str(self.to_mongo().to_dict())
        
    first_var = me.IntField()
    second_var = me.IntField()
    constraint = me.StringField(choices=constraints)
    operator = me.StringField(choices=operators)
    deviation = me.IntField()
    weight = me.IntField(default=0)
