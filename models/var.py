import mongoengine as me 


class Var(me.EmbeddedDocument):
    def __repr__(self):
        return str(self.to_mongo().to_dict())
        
    index = me.IntField()
    dom = me.IntField()
    initial = me.IntField()
    mobility = me.IntField()


