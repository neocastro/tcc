from mongoengine import connect
from os import walk
from models import Graph
from readers import file_to_ctrs, file_to_doms, file_to_vars


def populate_graph(graph_path):
    graph_dirs = next(walk(graph_path))[1]
    for graph_name in graph_dirs:
        basepath = f'{graph_path}/{graph_name}'
        ctrs = get_ctrs(basepath)
        doms = get_doms(basepath)
        variables = get_vars(basepath)
        Graph(ctr=ctrs,
              dom=doms,
              var=variables,
              name=graph_name).save()

def get_ctrs(path):
      filepath = f'{path}/ctr.txt'
      return file_to_ctrs(filepath)

def get_doms(path):
      filepath = f'{path}/dom.txt'
      return file_to_doms(filepath)

def get_vars(path):
      filepath = f'{path}/var.txt'
      return file_to_vars(filepath)

connect('tcc')
populate_graph('dataset/CELAR')
populate_graph('dataset/GRAPH')
populate_graph('dataset/SUBCELAR6')



