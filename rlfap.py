import mongoengine as me
from models import Graph
from z3 import *
import datetime
import math as math 


me.connect(db='tcc')

graph1 = next(Graph.objects(name='scen06')) 

Vars = graph1.var 
Doms = graph1.dom
Ctrs = graph1.ctr


costs = {
 1 : 1000,
 2 :  100,
 3 :   10,
 4 :    1
}


s = Optimize()
# s = SolverFor('QF_LIA')

# pre-assigned frequency constraints
# for var in Vars:
#     if var.initial:
#         if var.initial % 14 == 2:
#             s.add( Int('m_%d' % var.index) == (var.initial - 2)/14 )
#         else:
#             s.add( Int('m_%d' % var.index) == (var.initial - 8)/14 )

for ctr in Ctrs:
    
    ti = Bool('t_%d' % ctr.first_var) 
    tj = Bool('t_%d' % ctr.second_var)
    mi = Int('m_%d'  % ctr.first_var)
    mj = Int('m_%d'  % ctr.second_var)
    k  = ctr.deviation


    # Encoding the domain of Fi
    # ti  → (1  ≤ mi ≤ 11 ∨ 18 ≤ mi ≤ 28)
    # ¬ti → (29 ≤ mi ≤ 39 ∨ 46 ≤ mi ≤ 56)
    s.add(Implies(ti,      Or( And(1  <= mi, mi <= 11),  And(18 <= mi, mi <= 28))))
    s.add(Implies(Not(ti), Or( And(29 <= mi, mi <= 39),  And(46 <= mi, mi <= 56))))
    
    # Encoding the domain of Fj
    s.add(Implies(tj     , Or(And(1  <= mj, mj <= 11), And(18 <= mj, mj <= 28))))
    s.add(Implies(Not(tj), Or(And(29 <= mj, mj <= 39), And(46 <= mj, mj <= 56))))



    # Encoding the hard constraints  
    if ctr.operator == '=':
        # (ti ∧ ¬tj )
        s.add(Implies(And(ti,Not(tj)),   Or(mi - mj == math.floor((k+6)/14) + 1, mi - mj == math.ceil((-k+6)/14) -1 )))

        # (¬ti ∧ tj)
        s.add(Implies(And(Not(ti),tj),   Or(mi - mj == math.floor((k-6)/14) + 1, mi - mj == math.ceil((-k-6)/14) -1 )))
        
        # (ti ∧ tj)
        s.add(Implies(And(ti,tj),   Or(mi - mj == math.floor(k/14) + 1, mi - mj == math.ceil(-k/14) -1 )))

        # (¬ti ∧ ¬tj)
        s.add(Implies(And(Not(ti),Not(tj)),   Or(mi - mj == math.floor(k/14) + 1, mi - mj == math.ceil(-k/14) -1 )))


    # encoding the soft constraints
    if ctr.operator == '>':
        
        weight = costs[ctr.weight]

        # (ti ∧ ¬tj) 
        s.add_soft(
            Implies(And(ti, Not(tj)), Or( mi - mj >= math.floor((k+6)/14) + 1,  mi - mj <= math.ceil((-k+6)/14) -1 )),
            weight
        )
        #s.add(Implies(And(ti, Not(tj)), Or( mi - mj >= math.floor((k+6)/14) + 1,  mi - mj <= math.ceil((-k+6)/14) -1 )))

        # (¬ti ∧ tj)
        # s.add(Implies( And(Not(ti), tj), Or( mi - mj >= math.floor((k-6)/14) + 1,   mi - mj <= math.ceil((-k-6)/14) - 1 )))
        s.add_soft(
            Implies( And(Not(ti), tj), Or( mi - mj >= math.floor((k-6)/14) + 1,   mi - mj <= math.ceil((-k-6)/14) - 1 )),               
            weight
        )

        # (ti ∧ tj)
        # s.add(Implies(And(ti,tj) ,    Or( mi - mj == math.floor(k/14) + 1   ,      mi - mj == math.ceil(-k/14) -1   )))
        s.add_soft(
            Implies(And(ti,tj) ,    Or( mi - mj == math.floor(k/14) + 1   ,      mi - mj == math.ceil(-k/14) -1   )),
            weight
        )
 

        # (¬ti ∧ ¬tj)
        # s.add(Implies(And(Not(ti), Not(tj)), Or(mi - mj >= math.floor(k/14) + 1,    mi - mj <= math.ceil(-k/14) -1 )))
        s.add_soft(
            Implies(And(Not(ti), Not(tj)), Or(mi - mj >= math.floor(k/14) + 1,    mi - mj <= math.ceil(-k/14) -1 ))
            , weight
        )
 
    


print(s.check())

m = s.model()
print([ m.evaluate(o) for o in s.objectives() ])
print(s.statistics())


# for e in m:
#     print(e,m[e])
# for k, v in s.statistics():
#     print( "%s : %s" % (k, v))

